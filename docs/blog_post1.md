# Introducing i18n4go - Go language tool for internationalization (i18n)

## Abstract

* brief summary of CF CLI problem
* spike to get i18n added to CLI
* extracted i18n4go

## Introduction

* brief history of the tool
* what is lacking in existing i18n tools for Go lang
* why i18n is important

### Organization

* related tools
* architecture and design
* applying tool to small Go language project
* typical workflow
* conclusion and future

## Related Tools

* i18n tooling for Java and Ruby
* existing i18n tooling for Go language

## Architecture and Design

* i18n problem
* solving with tooling
* similar tools
* taking advantage of Go language's features

## Applying Tool

* using workflow to CF CLI
* using workflow to a small Go language program

## Typical Workflow

* applying to Go projects
* extracting strings
* merging strings
* rewriting code
* creating translations
* verifying translations
* maintaining

## Conclusion and Future

* recap of tool features
* using on CF CLI
* what is missing?
* immediate next steps

## References